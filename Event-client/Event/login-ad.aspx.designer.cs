﻿//------------------------------------------------------------------------------
// <généré automatiquement>
//     Ce code a été généré par un outil.
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </généré automatiquement>
//------------------------------------------------------------------------------

namespace Event {
    
    
    public partial class loginad {
        
        /// <summary>
        /// Contrôle form1.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;
        
        /// <summary>
        /// Contrôle txtemail.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtemail;
        
        /// <summary>
        /// Contrôle txtpass.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtpass;
        
        /// <summary>
        /// Contrôle remember.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox remember;
        
        /// <summary>
        /// Contrôle btnsign.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnsign;
        
        /// <summary>
        /// Contrôle lbMessage.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lbMessage;
    }
}
