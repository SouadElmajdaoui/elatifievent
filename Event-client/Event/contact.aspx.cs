﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace Event
{
    public partial class contact : System.Web.UI.Page
    {
        static string ch = WebConfigurationManager.ConnectionStrings["event"].ConnectionString;
        SqlConnection conex = new SqlConnection(ch);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnenvoyer_Click(object sender, EventArgs e)
        {
            try
            {
                conex.Open();
                string r1 = "insert into Message values (@nom,@email,@titre,@message)";
                SqlCommand cmd = new SqlCommand(r1, conex);
                cmd.Parameters.Add("@nom", SqlDbType.VarChar, 20);
                cmd.Parameters.Add("@email", SqlDbType.VarChar, 50);
                cmd.Parameters.Add("@titre", SqlDbType.VarChar, 50);
                cmd.Parameters.Add("@message", SqlDbType.VarChar, 300);

                cmd.Parameters[0].Value = txtnom.Text;
                cmd.Parameters[1].Value = textemail.Text;
                cmd.Parameters[2].Value = txttitre.Text;
                cmd.Parameters[3].Value = textmessage.Text;

                cmd.ExecuteNonQuery();
                res.Text = "L'envoie de message est faite";
                conex.Close();


            }
            catch (Exception ex)
            {
                Response.Write("Eroor" + ex.Message);
                res.Text = "L'envoie de message n'est pas faite";
            }
        }
    }
}