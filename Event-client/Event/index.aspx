﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Event.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form runat="server">

	<!-- Slider -->

	<div class="main_slider" style="background-image:url(images/slider_1.jpg)">
		<div class="container fill_height">
			<div class="row align-items-center fill_height">
				<div class="col">
					<div class="main_slider_content">
						<h6>Spring / Summer Collection 2019</h6>
						<h1>Our new Collection<br>2019</h1><br> <h3>Marrakech JUNE 17-20 </h3>

						<div class="red_button shop_now_button"><a href="register.aspx" style="color:White; text-decoration:none;">Register now</a></div>
						<div class="red_button shop_now_button">
							<a href="schedule.aspx" style="color:White; text-decoration:none;">More info</a></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Banner -->


	<div class="banner">
		<div class="container">
			<div class="col text-center">
					<div class="section_title new_arrivals_title">
						<h2>About</h2>
					</div>
				</div>
				<br><br>
			<center><p class="ABOUT--">  Our  concept : As a ‘multi-brand agency’ we offer a well balanced, extensive product portfolio of high quality, commercial collections targeting the modern 35 plus woman.   We are constantly researching the European fashion markets for modern, consistent and well performing labels, in order to keep our  brand portfolio up to date and to keep track of new market developments.  Agents for the UK and Ireland.</p> </center>
			<div class="row">
				<div class="col-md-4">
					<div class="banner_item align-items-center" style="background-image:url(images/banner_1.jpg)">
						<div class="banner_category">
							<a href="#">women's</a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="banner_item align-items-center" style="background-image:url(images/banner_2.jpg)">
						<div class="banner_category">
							<a href="#">accessories's</a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="banner_item align-items-center" style="background-image:url(images/banner_3.jpg)">
						<div class="banner_category">
							<a href="#">men's</a>
						</div>
					</div>
				</div>
			</div>
           

		</div>
	</div>

	<!-- ROLLANT/Carousel -->
	<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="margin: 60px!important;">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
  </ol><center>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="images/4k-wallpaper-boutique-clothes.jpg" class="d-block w-100" alt="..." style="height: 600px;">
    </div>
    <div class="carousel-item">
      <img src="images/classic-clothes-commerce-298863.jpg" class="d-block w-100" alt="..." style="height: 600px;">
    </div>
    <div class="carousel-item">
      <img src="images/adult-banking-black.jpg" class="d-block w-100" alt="..." style="height: 600px;">
    </div>
    <div class="carousel-item">
      <img src="images/produit.jpg" class="d-block w-100" alt="..." style="height: 600px; ">
    </div>
    <div class="carousel-item">
      <img src="images/banner_2.jpg" class="d-block w-100" alt="..."style="height: 600px;">
    </div>
    
  </div></center>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

	
            
                
                

						

						

	<!-- Deal of the week -->

	<div class="deal_ofthe_week">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-6">
					<div class="deal_ofthe_week_img">
						<img src="images/presenter.jpg" alt="" style="width:100%;">
					</div>
				</div>
				
				<div class="col-lg-6 text-right deal_ofthe_week_col">
					<div class="deal_ofthe_week_content d-flex flex-column align-items-center ">
						<div class="section_title">
							<h2>Deal Of The Week</h2>
						</div>
						<br><br>
						<iframe width="411" height="211" src="https://w2.countingdownto.com/2491465" frameborder="0"></iframe>






						
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Best Sellers -->

	<div class="best_sellers">
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="section_title new_arrivals_title">
						<h2>Our benefit</h2>
					</div>
				</div>
			</div>
		</div>

	<!-- Benefit -->

	<div class="benefit">
		<div class="container">
			<div class="row benefit_row">
				<div class="col-lg-3 benefit_col">
					<div class="benefit_item d-flex flex-row align-items-center">
						<div class="benefit_icon"><i class="fa fa-truck" aria-hidden="true"></i></div>
						<div class="benefit_content">
							<h6>free shipping</h6>
							<p>Suffered Alteration in Some Form</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 benefit_col">
					<div class="benefit_item d-flex flex-row align-items-center">
						<div class="benefit_icon"><i class="fa fa-money" aria-hidden="true"></i></div>
						<div class="benefit_content">
							<h6>cach on delivery</h6>
							<p>The Internet Tend To Repeat</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 benefit_col">
					<div class="benefit_item d-flex flex-row align-items-center">
						<div class="benefit_icon"><i class="fa fa-undo" aria-hidden="true"></i></div>
						<div class="benefit_content">
							<h6>45 days return</h6>
							<p>Making it Look Like Readable</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 benefit_col">
					<div class="benefit_item d-flex flex-row align-items-center">
						<div class="benefit_icon"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
						<div class="benefit_content">
							<h6>opening all week</h6>
							<p>8AM - 09PM</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Team -->

	<div class="blogs">
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="section_title">
						<h2>Our Team</h2>
					</div>
				</div>
			</div>
			<div class="row blogs_container">
				<div class="col-lg-4 blog_item_col">
					<div class="serv">
            					<img src="images/profil1.jpg" class="icon">
            					<h3 class="serv-title"> Amin Tazi</h3>
            					<p class="serv-desc"> Passionate Curious devloper </p>
            					
            				</div>
				</div>
				<div class="col-lg-4 blog_item_col">
					<div class="serv">
            					<img src="images/profil2.jpg" class="icon">
            					<h3 class="serv-title"> Oumaima Alami</h3>
            					<p class="serv-desc"> Passionate Curious of Marketing </p><br>
            					
            				</div>
				</div>
				<div class="col-lg-4 blog_item_col">
					<div class="serv">
            					<img src="images/profil3.jpg" class="icon">
            					<h3 class="serv-title"> Mohamed Eljazouli</h3>
            					<p class="serv-desc"> Passionate Curious of Marketing </p><br>
            					
            				</div>
				</div>
			</div>
		</div>
	</div>

	
<!--Partenaire -->
<div class="blogs">
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="section_title">
						<h2>Our partners</h2>
					</div>
				</div>
			</div>
			<br><br><br>
			<div class="row">
				<center><p class="ABOUT--">As a ‘full service agency’ we aim to develop and establish long term,  business partnerships with our retail partners. The needs of our customers are placed at the centre of our philosophy.  Customer service has the highest priority for us.</p></center>
				<br><br>
			</div>
			<div class="row">
              <div class="col-md-6">
    	         <div class="container">

    		        <img src="images/1_Rrku6ryTYdwkh605Yf3jsg.png" style="width: 91%;
    height: 226px;" class="img-responsive">
    		
    	          </div>
               </div>
               <div class="col-md-6">
			      <div class="container-fluid">
			       
                    <img src="images/nolcha-fashion-week-logo-web.jpg" style="width: 91%;"  class="img-responsive">
                   </div>
			
		       </div> 
		       </div>
            </div>	
	
<br><br><br><br><br>
	<!-- information -->
	
		<div >
			<div class="container">
            	<div class="row">
            		<div class="col text-center">
            			<div class="section_title">
            			   <h4 > Contact & Location</h4>
            			</div>
            		</div>
            	</div>
<br><br><br><br>
            	<div class="row">
            		
            			<div class=" col-lg-6 col-md-12">
                            <asp:TextBox ID="txtnom" runat="server" placeholder="Entrez votre nom..."  class="form-control input-lg"></asp:TextBox><br>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                ErrorMessage="*Remplissage du champ est important" ControlToValidate="txtnom" 
                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>

                            <asp:TextBox ID="textemail" runat="server" placeholder="Entrez votre adresse mail..." class="form-control input-lg" TextMode="Email"></asp:TextBox><br>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                ErrorMessage="*Remplissage de champ est important" 
                                ControlToValidate="textemail" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>

                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                ControlToValidate="textemail" Display="Dynamic" 
                                ErrorMessage="*format de email est incrrect" ForeColor="Red" 
                                ValidationExpression="[\w-\.]+@[\w\.]+\.{1}[\w]+"></asp:RegularExpressionValidator>

                            <asp:TextBox ID="txttitre" runat="server"  placeholder="Entrez le titre de votre message.." class="form-control input-lg"></asp:TextBox><br>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                ErrorMessage="*Remplissage de champ est important" ControlToValidate="txttitre" 
                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>

                            <asp:TextBox ID="textmessage" runat="server" rows="5" class="form-control input-lg" placeholder="Entrez votre message..."></asp:TextBox><br>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                ErrorMessage="*Remplissage de champ est important" 
                                ControlToValidate="textmessage" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator><br />
            				
                            <asp:Button ID="btnenvoyer" runat="server" class="btn btn-danger btn-lg" 
                                style="width: 100%;" Text="Envoyer votre message" onclick="btnenvoyer_Click" />
                            
            				<br>
                            <asp:Label ID="res" runat="server" ></asp:Label>
            			</div>
            			
            		<br><br><br><br>
            		    <div id="map" class="col-lg-6 col-md-12 ">

            			  <iframe style="width: 100%; height: 100%; " src="https://www.google.com/maps/d/embed?mid=1lIo9407DG_F3MNgNmpYWpxhcNMox_-9r" ></iframe>
            		    </div>


                
            </div>
            		
            	</div>
            </div>
        
<br><br>
<br>
<br>
<br>
<br>

</div>
</form>

</asp:Content>
