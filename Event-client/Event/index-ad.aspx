﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.Master" AutoEventWireup="true" CodeBehind="index-ad.aspx.cs" Inherits="Event.indexad" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">overview</h2>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-25">
                            <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c1">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-account-o"></i>
                                            </div>
                                            <div class="text">
                                            <h2><asp:Label ID="txtvisits" runat="server" ></asp:Label></h2>
                                                
                                                <span>Total Visits</span>
                                                
                                            </div>
                                        </div>
                                        <div class="overview-chart">
                                            <canvas id="widgetChart1"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c2">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-shopping-cart"></i>
                                            </div>
                                            <div class="text">
                                                <h2>
                                                    <asp:Label ID="Tregister" runat="server" Text="Label"></asp:Label></h2>
                                                <span>Total Registrations</span>
                                            </div>
                                        </div>
                                        <div class="overview-chart">
                                            <canvas id="widgetChart2"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c3">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-calendar-note"></i>
                                            </div>
                                            <div class="text">
                                                <h2>
                                                    <asp:Label ID="current" runat="server" Text="1"></asp:Label></h2>
                                                <span>Current Visitors</span>
                                            </div>
                                        </div>
                                        <div class="overview-chart">
                                            <canvas id="widgetChart3"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                
                            </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-6">
                                <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
                                    <div class="au-card-title" style="background-image:url('images/bg-title-02.jpg');">
                                        <div class="bg-overlay bg-overlay--blue"></div>
                                        <h3>
                                            <i class="zmdi zmdi-comment-text"></i>Recent registrations</h3>
                                        <button class="au-btn-plus">
                                            <i class="zmdi zmdi-plus"></i>
                                        </button>
                                    </div>
                                    <div class="au-inbox-wrap js-inbox-wrap">
                                        <div class="au-message js-list-load" style="max-height: -webkit-fill-available;">
                                            <div class="au-message__noti">
                                                <p>You Have <span>a</span> new registrations</p>
                                            </div>
                                            <div class="au-message-list" style="margin: 4%;">
                                                
                                                <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                                                    ConnectionString="<%$ ConnectionStrings:eventCollectionConnectionString %>" 
                                                    
                                                    SelectCommand="SELECT [photos], [name], [email], [job], [dateR] FROM [register]"></asp:SqlDataSource>  
                                                
                                                <asp:ListView ID="ListView1" runat="server" DataSourceID="SqlDataSource2" class="au-message js-list-load" style="max-height: -webkit-fill-available;">
                                                    <AlternatingItemTemplate>
                                                        <span style="">
                                                         <img src="<%# Eval("photos") %>" alt="" style="border-radius: 47.5px;" />
                                                        <br />
                                                       <asp:Label ID="nameLabel" runat="server" Text='<%# Eval("name") %>' />
                                                        <br />
                                                       <asp:Label ID="emailLabel" runat="server" Text='<%# Eval("email") %>' />
                                                        <br />
                                                       <asp:Label ID="jobLabel" runat="server" Text='<%# Eval("job") %>' />
                                                        <br />
                                                        <span style="margin-left:61%;" ><asp:Label ID="dateRLabel" runat="server" Text='<%# Eval("dateR") %>' /></span>
                                                        <br />
                                                        <br />
                                                        </span>
                                                      
                                                    </AlternatingItemTemplate>
                                                    <EditItemTemplate>
                                                        <span style="">
                                                        <img src="<%# Eval("photos") %>" alt="" style="border-radius: 47.5px;" />
                                                        <br />
                                                        <asp:TextBox ID="nameTextBox" runat="server" Text='<%# Bind("name") %>' />
                                                        <br />
                                                      <asp:TextBox ID="emailTextBox" runat="server" Text='<%# Bind("email") %>' />
                                                        <br />
                                                       <asp:TextBox ID="jobTextBox" runat="server" Text='<%# Bind("job") %>' />
                                                        <br />
                                                        <span style="margin-left:61%;" ><asp:TextBox ID="dateRTextBox" runat="server" Text='<%# Bind("dateR") %>' /></span>
                                                        <br />
                                                        <asp:Button ID="UpdateButton" runat="server" CommandName="Update" 
                                                            Text="Mettre à jour" />
                                                        <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" 
                                                            Text="Annuler" />
                                                        <br />
                                                        <br />
                                                        </span>
                                                    </EditItemTemplate>
                                                    <EmptyDataTemplate>
                                                        <span>Aucune donnée n&#39;a été retournée.</span>
                                                    </EmptyDataTemplate>
                                                    <InsertItemTemplate>
                                                        <span style="">photos:
                                                        <img src="<%# Eval("photos") %>" alt="" style="border-radius: 47.5px;" />
                                                        <br />
                                                        <!--name:-->
                                                        &nbsp;<asp:TextBox ID="nameTextBox" runat="server" Text='<%# Bind("name") %>' />
                                                        <br />
                                                        <!--email:-->
                                                        &nbsp;<asp:TextBox ID="emailTextBox" runat="server" Text='<%# Bind("email") %>' />
                                                        <br />
                                                        <!--job:-->
                                                        &nbsp;<asp:TextBox ID="jobTextBox" runat="server" Text='<%# Bind("job") %>' />
                                                        <br />
                                                        <!--dateR:-->
                                                        <span style="margin-left:61%;" ><asp:TextBox ID="dateRTextBox" runat="server"  Text='<%# Bind("dateR") %>' /></span>
                                                        <br />
                                                        <asp:Button ID="InsertButton" runat="server" CommandName="Insert" 
                                                            Text="Insérer" />
                                                        <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" 
                                                            Text="Désactiver" />
                                                        <br />
                                                        <br />
                                                        </span>
                                                    </InsertItemTemplate>
                                                    <ItemTemplate>
                                                        <span style="">
                                                        <img src="<%# Eval("photos") %>" alt="" style="border-radius: 47.5px;" />
                                                        <br />
                                                        <!--name:-->
                                                        &nbsp;<asp:Label ID="nameLabel" runat="server" Text='<%# Eval("name") %>' />
                                                        <br />
                                                         <!--job: -->
                                                        
                                                        &nbsp;<asp:Label ID="emailLabel" runat="server" Text='<%# Eval("email") %>' />
                                                        <br />
                                                         <!-- dateR: -->
                                                       
                                                        &nbsp;<asp:Label ID="jobLabel" runat="server" Text='<%# Eval("job") %>' />
                                                        <br />
                                                       
                                                        <span style="margin-left:61%;" ><asp:Label ID="dateRLabel" runat="server" Text='<%# Eval("dateR") %>' /></span>
                                                        <br />
                                                        <br />
                                                        </span>
                                                       
                                                    </ItemTemplate>
                                                    <LayoutTemplate>
                                                        <div 
                                                            style="" ID="itemPlaceholderContainer" runat="server">
                                                            <span runat="server" id="itemPlaceholder" />
                                                        </div>
                                                        <div style="">
                                                            <asp:DataPager ID="DataPager1" runat="server">
                                                                <Fields>
                                                                    <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" 
                                                                        ShowNextPageButton="False" ShowPreviousPageButton="False" />
                                                                    <asp:NumericPagerField />
                                                                    <asp:NextPreviousPagerField ButtonType="Button" ShowLastPageButton="True" 
                                                                        ShowNextPageButton="False" ShowPreviousPageButton="False" />
                                                                </Fields>
                                                            </asp:DataPager>
                                                        </div>
                                                    </LayoutTemplate>
                                                    <SelectedItemTemplate>
                                                        <span style="">
                                                        <img src="<%# Eval("photos") %>" alt="" style="border-radius: 47.5px;" />
                                                        <asp:Label ID="nameLabel" runat="server" Text='<%# Eval("name") %>' />
                                                        <br />
                                                      <asp:Label ID="emailLabel" runat="server" Text='<%# Eval("email") %>' />
                                                        <br />
                                                       <asp:Label ID="jobLabel" runat="server" Text='<%# Eval("job") %>' />
                                                        <br />
                                                       <span style="margin-left:61%;" ><asp:Label ID="dateRLabel" runat="server" Text='<%# Eval("dateR") %>' /></span>
                                                        <br />
                                                        <br />
                                                        </span>
                                                        
                                                      
                                                    </SelectedItemTemplate>
                                                </asp:ListView>
                                                
                                                
                                            </div>
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="au-card chart-percent-card" style="height: 94%;">
                                    <div class="au-card-inner">
                                        <h3 class="title-2 tm-b-5">Web tracking Analytics</h3>
                                        <div class="row no-gutters">
                                            <div class="col-xl-6">
                                                <div class="chart-note-wrap">
                                                    <div class="chart-note mr-0 d-block">
                                                        <span class="dot dot--blue"></span>
                                                        <span>Landing page Bounce Rate</span>
                                                    </div>
                                                    <div class="chart-note mr-0 d-block">
                                                        <span class="dot dot--red"></span>
                                                        <span>Register page Conversion Rate</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="percent-chart">
                                                    <canvas id="percent-chart"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                            </div>
                        </div>
                        
                           
                                <h2 class="title-1 m-b-25">Recent Message</h2>
                                <div class="table-responsive table--no-card m-b-40">
                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:eventCollectionConnectionString2 %>" 
                                        
                                        SelectCommand="SELECT [nom], [email], [titremsg], [message], [dateM] FROM [Message]"></asp:SqlDataSource>
                                        <asp:GridView ID="GridView1"  
                                        class="table table-borderless table-striped table-earning" runat="server" 
                                        
                                        AutoGenerateColumns="False" DataSourceID="SqlDataSource1" 
                                        BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" 
                                        CellPadding="4" CellSpacing="2" ForeColor="Black">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Name" SortExpression="nom">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("nom") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("nom") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Email" SortExpression="email">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("email") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("email") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Title" SortExpression="titremsg">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("titremsg") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("titremsg") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Message" SortExpression="message">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("message") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("message") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Date" SortExpression="dateM">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("dateM") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("dateM") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <FooterStyle BackColor="#CCCCCC" />
                                            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Center" />
                                            <RowStyle BackColor="White" />
                                            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                            <SortedAscendingHeaderStyle BackColor="#808080" />
                                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                            <SortedDescendingHeaderStyle BackColor="#383838" />
                                        </asp:GridView>
                                  
                                </div>
                        
                            
                        
                       </div>
                       
                        
                   
                </div>
            </div>
</asp:Content>
