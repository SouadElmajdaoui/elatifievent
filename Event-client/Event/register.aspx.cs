﻿     using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

using System.Net.Mail;
using System.Net;
using System.IO;

namespace Event
{
    public partial class register : System.Web.UI.Page
    {
        static string ch = WebConfigurationManager.ConnectionStrings["event"].ConnectionString;
        SqlConnection conex = new SqlConnection(ch);
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnregister_Click(object sender, EventArgs e)
        {
            try
            {
                conex.Open();
                


                string path = Server.MapPath("register-img/");
                if (FileUpload1.HasFile)
                {
                    string ext = Path.GetExtension(FileUpload1.FileName);
                    if (ext==".jpg"||ext==".png")
                    {
                        FileUpload1.SaveAs(path + FileUpload1.FileName);
                        string name = "register-img/" + FileUpload1.FileName;

                        string r1 = "insert into register values (@email,@name,@pass,@phone,@job,@img,@date)";
                        SqlCommand cmd = new SqlCommand(r1, conex);
                        cmd.Parameters.Add("@email", SqlDbType.VarChar, 50);
                        cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
                        cmd.Parameters.Add("@pass", SqlDbType.VarChar, 20);
                        cmd.Parameters.Add("@phone", SqlDbType.VarChar, 15);
                        cmd.Parameters.Add("@job", SqlDbType.VarChar, 50);
                        cmd.Parameters.Add("@img", SqlDbType.VarChar, 50);
                        cmd.Parameters.Add("@date", SqlDbType.Date, 8);


                        cmd.Parameters[0].Value = txtemail1.Text;
                        cmd.Parameters[1].Value = textname.Text;
                        cmd.Parameters[2].Value = txtpass1.Text;
                        cmd.Parameters[3].Value = txtphone.Text;
                        cmd.Parameters[4].Value = txtjob.Text;
                        cmd.Parameters[5].Value = name;
                        cmd.Parameters[6].Value = System.DateTime.Now;

                        cmd.ExecuteNonQuery();
                        res.Text = "Votre enregistrement est faite avec succé";
                    }
                    else
                    {
                        res.Text = "*You have to upload jpg or png file only! Thank you";
                    }


                }
                else
                {
                    res.Text = "*You have to upload jpg or png file only! Thank you";
                }
               
                
                
                Send sms = new Send();
                sms.sendSMS(txtphone.Text);
            }
            catch (Exception ex)
            {

                Response.Write(ex.Message);
                res.Text = ex.Message;
            }
            finally
            { 
                conex.Close(); 
            }

            try
            {
                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                MailAddress adresseMailEnvoyeur = new MailAddress("elatifi.oumaima@gmail.com");
                MailAddress Receveur = new MailAddress(txtemail1.Text);
                MailMessage ConfigMess = new MailMessage(adresseMailEnvoyeur, Receveur);
                ConfigMess.Body = "Hello votre registration est faite juste un moment pour avoir est ce qu'il accepté ou pas.Merci";
                ConfigMess.Subject = "Sujet";
                //ConfigMess.IsBodyHtml = true; // si tu veux activer le html dans ton body
                //ConfigMess.Body = body;
                NetworkCredential user = new NetworkCredential("elatifi.oumaima@gmail.com", "pass");
                client.Credentials = user;
                client.Send(ConfigMess);

            }
            catch (Exception ex)
            {
                Response.Write("Eroor" + ex.Message);
            }
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
             string data = args.Value;
            args.IsValid = false;
            double filesize = FileUpload1.FileContent.Length;
            if (filesize > 50000000)
            {
                args.IsValid = false;
                CustomValidator1.ErrorMessage = "Taille de image est invalide";
            }
            else
            {
                string fileExtension = Path.GetExtension(FileUpload1.FileName);
                if (fileExtension.ToLower()==".jpg" || fileExtension.ToLower()==".png")
                {
                    args.IsValid = true;


                    string path = Server.MapPath("register-img/" + FileUpload1.FileName);
                    FileUpload1.SaveAs(path);
                    CustomValidator1.ErrorMessage = "Taille et format de image est valide";
                }
                else
                {
                    args.IsValid = false;
                    CustomValidator1.ErrorMessage = "Format de image est invalide";
                }
                
            }
        }
    }
}