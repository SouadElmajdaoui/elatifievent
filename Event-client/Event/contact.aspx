﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="contact.aspx.cs" Inherits="Event.contact" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<br><br>
<br>
<br>
<form runat="server">
<br><br>
<br>
<br>
       <!-- information -->
	
		<div >
			<div class="container">
            	<div class="row">
            		<div class="col text-center">
            			<div class="section_title">
            			   <h4 > Contact & Location</h4>
            			</div>
            		</div>
            	</div>
<br><br><br><br>
            	<div class="row">
            		
            			<div class=" col-lg-6 col-md-12">
                            <asp:TextBox ID="txtnom" runat="server" placeholder="Entrez votre nom..."  class="form-control input-lg"></asp:TextBox><br>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                ErrorMessage="*Remplissage du champ est important" ControlToValidate="txtnom" 
                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>

                            <asp:TextBox ID="textemail" runat="server" placeholder="Entrez votre adresse mail..." class="form-control input-lg" TextMode="Email"></asp:TextBox><br>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                ErrorMessage="*Remplissage de champ est important" 
                                ControlToValidate="textemail" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>

                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                ControlToValidate="textemail" Display="Dynamic" 
                                ErrorMessage="*format de email est incrrect" ForeColor="Red" 
                                ValidationExpression="[\w-\.]+@[\w\.]+\.{1}[\w]+"></asp:RegularExpressionValidator>

                            <asp:TextBox ID="txttitre" runat="server"  placeholder="Entrez le titre de votre message.." class="form-control input-lg"></asp:TextBox><br>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                ErrorMessage="*Remplissage de champ est important" ControlToValidate="txttitre" 
                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>

                            <asp:TextBox ID="textmessage" runat="server" rows="5" class="form-control input-lg" placeholder="Entrez votre message..."></asp:TextBox><br>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                ErrorMessage="*Remplissage de champ est important" 
                                ControlToValidate="textmessage" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator><br />
            				
                            <asp:Button ID="btnenvoyer" runat="server" class="btn btn-danger btn-lg" 
                                style="width: 100%;" Text="Envoyer votre message" 
                                onclick="btnenvoyer_Click"  />
                            
            				<br>
                            <asp:Label ID="res" runat="server" ></asp:Label>
            			</div>
            			
            		<br><br><br><br>
            		    <div id="map" class="col-lg-6 col-md-12 ">

            			  <iframe style="width: 100%; height: 100%; " src="https://www.google.com/maps/d/embed?mid=1lIo9407DG_F3MNgNmpYWpxhcNMox_-9r" ></iframe>
            		    </div>


                
            </div>
            		
            	</div>
            </div>
        

<br>
<br>
</form>
</asp:Content>
