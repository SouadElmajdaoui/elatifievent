﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Event.login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="Form1" runat="server">
<!-- Slider -->
	<div class="main_slider" style="background-image:url(images/login.jpg)">
		<div class="container fill_height">
			<div class="row align-items-center fill_height">
				<div class="col">
					<div class="main_slider_content">
						<h6 style="color: rgb(255 128 128);">Spring / Summer Collection 2019</h6>
						<h1 style="color: rgb(255 128 128);">Our new Collection in Marrakech<br>2019</h1><br> <h3 style=" color: white"> Sign in </h3>
						
					</div>
				</div>
			</div>
		</div>
	</div>

<!--inoformation of register -->
<br><br><br><br>
<div class="container" >
	<center>
		<p class="ABOUT--" style="font-size: 30px"><span style="color: rgb(255 128 128);">*</span>make your connection after your registration</p>
        <div class="input-group mb-3">
    <asp:TextBox ID="txtemail1" class="form-control" placeholder="Email" aria-label="Recipient's username" aria-describedby="basic-addon2" runat="server" TextMode="Email"></asp:TextBox>
    <div class="input-group-append">
    <span class="input-group-text" id="basic-addon2">@example.com</span>
  </div>
  <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
        ErrorMessage="*Remlissage du champ est important" ControlToValidate="txtemail1" 
        Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
        ErrorMessage="*syntax de email est incorrect" ControlToValidate="txtemail1" 
        Display="Dynamic" ForeColor="Red" 
        ValidationExpression="[\w-\.]+@[\w\.]+\.{1}[\w]+"></asp:RegularExpressionValidator>
  
</div>
<div class="input-group mb-3">
    <asp:TextBox ID="txtpass1" class="form-control" placeholder="Password" aria-label="Username" aria-describedby="basic-addon1" runat="server" TextMode="Password"></asp:TextBox><br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
        ErrorMessage="*Remlissage du champ est important" ControlToValidate="txtpass1" 
        Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
        ErrorMessage="*Syntax de mot de passe est incorrect" 
        ControlToValidate="txtpass1" Display="Dynamic" ForeColor="Red" 
        ValidationExpression="^(?=.*[a-z])(?=.*\d)(?=.*[A-Z])(?=.*[@$!%*?&amp;])[A-Za-z\d@$!%*?&amp;]{8,}$"></asp:RegularExpressionValidator>
</div>
<asp:Button ID="btnregister"  runat="server" Text="Sign up" 
            class="btn btn-danger btn-lg" style="width: 78%;" 
            onclick="btnregister_Click"  /> <br />
             <asp:Label ID="res" runat="server" ></asp:Label> 
       
<br>

<span class="ABOUT--">OR</span>

<a href="#" class="btn btn-block btn-social btn-facebook" style="width: 64%; background-color: rgb(0 0 128); color: white;">
    <span class="fa fa-facebook"></span> Continue with facebook
  </a>
  <a href="#" class="btn btn-block btn-social btn-google" style="width: 64%; background-color: rgb(206 0 0); color: white;">
    <span class="fa fa-google"></span> Sign up with google+
  </a>








	</center>
	
</div>
<br><br><br><br>


</form>
</asp:Content>
