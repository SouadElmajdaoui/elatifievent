﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="schedule.aspx.cs" Inherits="Event.schedule" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!-- Slider -->
<form runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
    
	<div class="main_slider" style="background-image:url(images/slider_1.jpg)">
		<div class="container fill_height">
			<div class="row align-items-center fill_height">
				<div class="col">
					<div class="main_slider_content">
					<h1>Our new Collection<br><span style="color: rgb(255 128 128);">2019</h1></span><br> <h3>Marrakech<span style="color: rgb(255 128 128);"> JUNE 17-19 </span> </h3>
                    <h6 >SCHEDULE & TRAUEL INFO</h6>
						
					</div>
				</div>
			</div>
		</div>
	</div>

    <center>
		<p class="ABOUT--" style="font-size: 30px"><span style="color: rgb(255 128 128);">*</span>SCHEDULE</p>
		<p>EVENT TIME TABLE</p>
	</center>

	<div class=" container">
	   <div class="container">

	    <div class="row">
			<div class="col-md-4">
              <div class="btn programe btn-outline-warning days_event"  style="border: none;width: 100%;">
                 <a href="#1" id="c1"  style="color:brown;text-decoration:none; font-style:italic;font-weight:bold;">17/06</a>
               </div>
              </div>
            <div class="col-md-4">
              <div class="btn programe btn-outline-warning days_event "  style="border: none;width: 100%;">
                   <a href="#2" id="c2" style="color:brown;text-decoration:none; font-style:italic; font-weight:bold;"> 18/06</a>
                </div>
             </div>
            <div class="col-md-4">
               <div class="btn programe btn-outline-warning days_event"  style="border: none;width: 100%;">
                    <a href="#3" id="c3" style="color:brown;text-decoration:none; font-style:italic;font-weight:bold;">19/06</a>
                </div>
             </div>
        </div>
     </div>
		<hr>
        <div class="container">
         <div class="row enents">
           <div class="col-lg-12" >
             <center><div class="avatar" id="avatar"></div></center>
             
             <div id="1" style="margin-left: 17%;">
             <h3 class="titre">Mohamed tazi</h3><br />
             <img src="images/profil1.jpg" alt="" style="width: 11%;height: 11%;border-radius: 52.5px;margin-left: -15%;" />
             <p id="desc" style="font-size:18px;margin-left: 6%; margin-top: -17%;">This is a list of fashion-related events<br /> and shows happening during February/March and September/October in Paris.<br /> It includes Paris Fashion Week shows held by the FHCM <br /> (Fédération de la Haute Couture et de la Mode), as well as unaffiliated independent<br /> fashion events for the general public.
             <br />
Special opportunities are in <span style="color:Green;" >green </span>.<br />
Live streams are in <span style="color:Orange;" > orange</span>.</p></p>
             <div class="date_hour"><span id="date" style="margin-left: 61%; color: rgb(255 128 128);font-weight:bold;">17/06/2019</span><span id="heur" style="margin-left: 1%; color: rgb(255 128 128);font-weight:bold;">07:00pm-10:00pm</span> <span id="location" style="margin-left: 1%;font-weight:bold;color: rgb(255 128 128);">Salle1</span></div>
             </div>
           </div>
         </div>
        
        </div>



        <div class="container">
         <div class="row enents">
           <div class="col-lg-12" >
             <center><div class="avatar" id="Div1"></div></center>
             <div id="2" style="display:none; margin-left: 17%;">
             <h3 class="titre">Amin tazi</h3><br />
             <img src="images/profil3.jpg" alt="" style="width: 11%; height: 11%;border-radius: 52.5px;margin-left: -15%;" />
             <p id="P1" style="font-size:18px;margin-left: 6%; margin-top: -8%;">This is a list of fashion-related events<br /> and shows happening during February/March and September/October in Paris.<br /> It includes Paris Fashion Week shows held by the FHCM <br /> (Fédération de la Haute Couture et de la Mode), as well as unaffiliated independent<br /> fashion events for the general public.
             <br />
Special opportunities are in <span style="color:Green;" >green </span>.<br />
Live streams are in <span style="color:Orange;" > orange</span>.</p>
             <div class="date_hour"><span id="Span1" style="margin-left: 61%; color: rgb(255 128 128);font-weight:bold;">18/06/2019</span><span id="Span2" style="margin-left: 1%; color: rgb(255 128 128);font-weight:bold;">09:00pm-00:00</span> <span id="Span3" style="margin-left: 1%;color: rgb(255 128 128);font-weight:bold;">Salle1</span></div>
             </div>
           </div>
         </div>
        
        </div>



        <div class="container">
         <div class="row enents">
           <div class="col-lg-12" >
             <center><div class="avatar" id="Div3"></div></center>
             <div id="3" style="display:none; margin-left: 17%;">
             <h3 class="titre">Salam tazi</h3>
             <br />
             <img src="images/profil2.jpg" alt="" style="width: 11%; height: 11%;border-radius: 52.5px;margin-left: -15%;" />
             <p id="P2" style="font-size:18px;margin-left: 6%;margin-top: -8%;">This is a list of fashion-related events<br /> and shows happening during February/March and September/October in Paris.<br /> It includes Paris Fashion Week shows held by the FHCM <br /> (Fédération de la Haute Couture et de la Mode), as well as unaffiliated independent<br /> fashion events for the general public.
             <br />
Special opportunities are in <span style="color:Green;" >green </span>.<br />
Live streams are in <span style="color:Orange;" > orange</span>.</p></p>
             <div class="date_hour"><span id="Span4" style="margin-left: 61%;color: rgb(255 128 128); font-weight:bold;">19/06/2019</span><span id="Span5" style="margin-left: 1%;color: rgb(255 128 128);font-weight:bold;"> 10:00pm-02:00</span> <span id="Span6" style="margin-left: 1%;color: rgb(255 128 128);font-weight:bold;">Salle1</span></div>
             </div>
           </div>
         </div>
        
        </div>
       
</div>
<!-- google map -->
<div class="container">
<center>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
   
     <div>
        <b>Start: </b>
        <select id="start" ONCHANGE="document.getElementById('youriframe').src = this.options[this.selectedIndex].value">
          <option value="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d27180.839232824903!2d-8.009642239575511!3d31.617284230596105!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0xdafeed09eb0c575%3A0xc3f5815b7b3d9fe7!2sMarrakesh+Menara+Airport!3m2!1d31.604788099999997!2d-8.0210505!4m5!1s0xdafee8d96179e51%3A0x5950b6534f87adb8!2sMarrakesh!3m2!1d31.6294723!2d-7.9810845!5e0!3m2!1sen!2sma!4v1553479086221">Airport</option>
          <option value="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d22853.731035855133!2d-8.011770807973095!3d31.627622418208137!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e0!4m5!1s0xdafee9465ae0bf7%3A0x27332ef8ec3bea85!2sMarrakech!3m2!1d31.629793!2d-8.019043!4m5!1s0xdafee8d96179e51%3A0x5950b6534f87adb8!2sMarrakesh!3m2!1d31.6294723!2d-7.9810845!5e0!3m2!1sen!2sma!4v1553479229589">Train Station</option>

        </select>
        <b>End: </b>
        <select id="end">
          <option value="chicago, il">Marrakech</option>
        </select>
        </div>
    <iframe src="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d27180.839232824903!2d-8.009642239575511!3d31.617284230596105!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0xdafeed09eb0c575%3A0xc3f5815b7b3d9fe7!2sMarrakesh+Menara+Airport!3m2!1d31.604788099999997!2d-8.0210505!4m5!1s0xdafee8d96179e51%3A0x5950b6534f87adb8!2sMarrakesh!3m2!1d31.6294723!2d-7.9810845!5e0!3m2!1sen!2sma!4v1553479086221" id="youriframe" width="97%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </center>
    </div>
	<script type="text/javascript" src="js/jquery-3.2.1.min.js">
    </script>
    <script type="text/javascript" src="js/ajaxActicte.js"></script>
    
        
    
    </form>

    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#c1').click(function () {
                $('#1').show();
                $('#2').hide();
                $('#3').hide();

            });
            $('#c2').click(function () {
                $('#2').show();
                $('#1').hide();
                $('#3').hide();

            });
            $('#c3').click(function () {
                $('#3').show();
                $('#1').hide();
                $('#2').hide();

            });


        });
    
    </script>
</asp:Content>


