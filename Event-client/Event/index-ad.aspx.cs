﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace Event
{
    public partial class indexad : System.Web.UI.Page
    {
        static string ch = WebConfigurationManager.ConnectionStrings["event"].ConnectionString;
        SqlConnection conex = new SqlConnection(ch);
        protected void Page_Load(object sender, EventArgs e)
        {
            conex.Open();
            string rq1 = "select count(*) from register";
            SqlCommand cmd = new SqlCommand(rq1, conex);

            Tregister.Text = cmd.ExecuteScalar().ToString();

            string rq2 = "select total from visitors";
            SqlCommand cmd2 = new SqlCommand(rq2, conex);
            txtvisits.Text = cmd2.ExecuteScalar().ToString();
        }
    }
}