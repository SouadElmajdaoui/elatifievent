﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

using System.Net.Mail;
using System.Net;


namespace Event
{
    public partial class index : System.Web.UI.Page
    {
        static string ch = WebConfigurationManager.ConnectionStrings["event"].ConnectionString;
        SqlConnection conex = new SqlConnection(ch);
        protected void Page_Load(object sender, EventArgs e)
        {
            Application.Lock();
            Application["comp"] = (int)Application["comp"] + 1;
            Application.UnLock();
            conex.Open();
            string rq = "insert into visitors values(@total)";
            SqlCommand cmd2 = new SqlCommand(rq, conex);
            cmd2.Parameters.Add("@total", SqlDbType.Int, 32);
            cmd2.Parameters[0].Value = Application["compv"].ToString();
            cmd2.ExecuteNonQuery();
            conex.Close();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //register now
        }

        protected void btnenvoyer_Click(object sender, EventArgs e)
        {
            try
            {
                conex.Open();
                string r1 = "insert into Message values (@nom,@email,@titre,@message,@date)";
                SqlCommand cmd = new SqlCommand(r1, conex);
                cmd.Parameters.Add("@nom", SqlDbType.VarChar, 20);
                cmd.Parameters.Add("@email", SqlDbType.VarChar, 50);
                cmd.Parameters.Add("@titre", SqlDbType.VarChar, 50);
                cmd.Parameters.Add("@message", SqlDbType.VarChar, 300);
                cmd.Parameters.Add("@date", SqlDbType.Date, 8);

                cmd.Parameters[0].Value = txtnom.Text;
                cmd.Parameters[1].Value = textemail.Text;
                cmd.Parameters[2].Value = txttitre.Text;
                cmd.Parameters[3].Value = textmessage.Text;
                cmd.Parameters[4].Value = System.DateTime.Now;

                cmd.ExecuteNonQuery();
                res.Text = "L'envoie de message est faite";
                conex.Close();


            }
            catch (Exception ex)
            {
                Response.Write("Eroor" + ex.Message);
                res.Text = "L'envoie de message n'est pas faite";
            }
        }
    }
}