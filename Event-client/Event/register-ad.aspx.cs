﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.Security;


namespace Event
{
    public partial class registerad : System.Web.UI.Page
    {
        static string ch = WebConfigurationManager.ConnectionStrings["event"].ConnectionString;
        SqlConnection conex = new SqlConnection(ch);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnregister_Click(object sender, EventArgs e)
        {
            try
            {
                conex.Open();
                string r1 = "insert into admin values (@user,@email,@pass)";
                SqlCommand cmd = new SqlCommand(r1, conex);
                cmd.Parameters.Add("@user", SqlDbType.VarChar, 50);
                cmd.Parameters.Add("@email", SqlDbType.VarChar, 50);
                cmd.Parameters.Add("@pass", SqlDbType.VarChar, 20);


                cmd.Parameters[0].Value = txtuser.Text;
                cmd.Parameters[1].Value = txtemail.Text;
                cmd.Parameters[2].Value = txtpass.Text;

                cmd.ExecuteNonQuery();
                res.Text = "Votre enregistrement est faite avec succé";
            }
            catch (Exception ex)
            {

                Response.Write(ex.Message);
                res.Text = "Votre enregistrement n'est pas faite"+ex.Message;
            }
            finally
            {
                conex.Close();
            }
        }
    }
}